#!/bin/bash

source ./kukulkan.conf

##
# This script initializes the container environment and creates the container 

echo "creating container $containerName (if not yet exists)"
lxc init images:$containerDistro/$containerDistroVersion $containerName

# rather no container then a kukulkan not logging!
lxc config set $containerName boot.autostart false

source ./startGuest.sh

echo "updating guest OS"
lxc exec $containerName -- apt-get update
lxc exec $containerName -- apt-get upgrade -y

# every containers idmap-range is isolated from every other containers range
lxc config set $containerName security.idmap.isolated true

# TODO: genauere Ausgabe
# echo "ID MAP for container $containerName:"
# lxc config get u1 volatile.idmap.base
# lxc config get u1 volatile.last_state.idmap


echo "limit container"
echo "cpu: $containerLimitCpuAllowance"
echo "memory: $containerLimitMemory"
lxc config set $containerName limits.cpu.allowance $containerLimitCpuAllowance
lxc config set $containerName limits.memory $containerLimitMemory

echo "Network: down:$containerLimitNetworkIngress up:$containerLimitNetworkEgress"
lxc config device add $containerName eth0 nic name=eth0 nictype=bridged parent=lxdbr0
lxc config device set $containerName eth0 limits.ingress $containerLimitNetworkIngress 
lxc config device set $containerName eth0 limits.egress $containerLimitNetworkEgress


echo "SKIP creating snapshot"
#lxc snapshot $containerName $containerSnapshotName
