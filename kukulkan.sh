#!/bin/bash

##
# Kukulkan
# Preapares your table for human sacrifices

## FIRST
# execute initHost.sh manually! beware of the ssh port-change!
# TODO: Status irgendwo ablegen, ob init bereits ausgeführt wurde oder nicht, wenn nein, ausführen, sonst nicht

source ./kukulkan.conf

git pull

# echo "Please add deployment key $hostDeploymentKeyFile to repository $webserverGit"
# cat $hostDeploymentKeyFile'.pub'

# read -p "Added? [Enter]"


lxc delete $containerName --force &>/dev/null

source ./initGuest.sh # TODO: prüfen, ob bereits initialisiert wurde...

source ./startGuest.sh # if not yet running

source ./populateGuestApacheWebsite.sh

source ./startSurveilance.sh
