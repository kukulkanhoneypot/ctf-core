#!/bin/bash

source ./kukulkan.conf

## ATTENTION!
# This script changes the port of the ssh-deamon!
# After running this script and after restarting the host and/or ssh-deamon
# any new connection must use the new port!

source ./initHostSurveillance.sh

source ./initHostContainment.sh


# ATTENTION see above
# Change the host-port to the port stated in kukulkan.conf
sed -i -e 's/#Port 22/Port '$hostSshListeningPort'/g' $configFileSSHD

echo "ATTENTION"
echo "HOST-SSH-PORT CHANGED TO $hostSshListeningPort"
echo "Any new connection after restart must use this port!"

service sshd restart
