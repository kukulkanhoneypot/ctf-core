#!/bin/bash

source ./kukulkan.conf

## 
# This script installs the services inside the container

source ./initGuestDistribution.sh

source ./initGuestSshd.sh

source ./initGuestApache.sh

if $guestApacheUseHttps;
then
    source ./initGuestApacheHttps.sh
fi
