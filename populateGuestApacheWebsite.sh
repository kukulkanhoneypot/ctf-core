#!/bin/bash

## fills website with data from git

source ./kukulkan.conf

# ubuntu/apache default
htmlFoldername="html/" 

rm -rf $htmlFoldername
mkdir -p $htmlFoldername
git clone $webserverGit $htmlFoldername

# remove .git folder before copying into guest!
## DISABLED FOR CTF
##rm -rf $htmlFoldername'.git/'
## DISABLED FOR CTF

# clean guest
lxc exec $containerName -- rm -rf '/var/www/'$htmlFoldername

echo "copy files to guest"
lxc file push $htmlFoldername $containerName/var/www/ -r
lxc exec $containerName -- chmod 777 /var/www/$htmlFoldername/useruploads/

echo "copied files to guest"


echo "copy flags to guest"

lxc file push ./flagContainerUser.txt $containerName/flag.txt
lxc exec $containerName -- chmod 777 /flag.txt

lxc file push ./flagContainerRoot.txt $containerName/root/flag.txt
lxc exec $containerName -- chmod 700 /root/flag.txt

echo "copied flags to guest"



echo "copy flags on Host"

cp ./flagHostUser.txt /flag.txt
chmod 777 /flag.txt

cp ./flagHostRoot.txt /root/flag.txt
chmod 700 /root/flag.txt

echo "copied flags on Host"
