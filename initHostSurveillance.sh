#!/bin/bash

source ./kukulkan.conf

##
# Set up the surveillance
# ref: https://github.com/draios/sysdig/wiki/How-to-Install-Sysdig-for-Linux

curl -s https://s3.amazonaws.com/download.draios.com/DRAIOS-GPG-KEY.public | sudo apt-key add -  
curl -s -o /etc/apt/sources.list.d/draios.list https://s3.amazonaws.com/download.draios.com/stable/deb/draios.list  
apt-get update

apt-get -y install linux-headers-$(uname -r) sysdig 

# installs further needed commands (separated command for clarification)
apt-get -y install screen
