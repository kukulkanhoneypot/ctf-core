#!/bin/bash

## installs apache inside the container

source ./kukulkan.conf

lxc exec $containerName -- apt-get update
lxc exec $containerName -- apt-get install -y apache2 php
