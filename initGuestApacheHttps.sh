#!/bin/bash

## (interactively) configures apache inside the container to use https

source ./kukulkan.conf

lxc exec $containerName -- apt-get update
lxc exec $containerName -- apt-get install -y software-properties-common
lxc exec $containerName -- add-apt-repository -y universe
lxc exec $containerName -- add-apt-repository -y ppa:certbot/certbot
lxc exec $containerName -- apt-get update

lxc exec $containerName -- apt-get install -y certbot python3-certbot-apache


lxc file push ./letsencrypt.tar $containerName/root/
lxc exec $containerName -- tar -xpf /root/letsencrypt.tar -C /etc/

# interactive script!
instanceName=$(hostname) # HOSTNAME OF THE HOST! IT IS DIFFERENT IN THE GUEST
lxc exec $containerName -- certbot install --non-interactive --cert-name kukulkanctf.de --apache -d $instanceName.kukulkanctf.de

# certbot macht das bei --non-interactive nicht alleine ...

lxc exec $containerName -- sh -c 'sed -i "29,31d" /etc/apache2/sites-available/000-default.conf'

lxc exec $containerName -- sh -c 'echo "  #Alles an SSl weiterleiten" >> /etc/apache2/sites-available/000-default.conf'
lxc exec $containerName -- sh -c 'echo "  RewriteEngine on" >> /etc/apache2/sites-available/000-default.conf'
lxc exec $containerName -- sh -c 'echo "  RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]" >> /etc/apache2/sites-available/000-default.conf'
lxc exec $containerName -- sh -c 'echo "</VirtualHost>" >> /etc/apache2/sites-available/000-default.conf'

lxc exec $containerName -- a2enmod rewrite
lxc exec $containerName -- systemctl restart apache2

