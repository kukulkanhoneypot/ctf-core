## ANGEPASSTE VERSION FÜR CTF 

⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠
⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠
⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠
⚠ ⚠
⚠ ⚠ ANGEPASSTE VERSION FÜR CTF - Enthält private Key!
⚠ ⚠
⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠
⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠
⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠ ⚠

# Kukulkan

A containerized full system high interaction honeypot with shell monitoring.

Kukulkan is a mayan-god and ancient civilizations gave human sacrifices to him.
We do the same with attackers who touch our honeypot.


## Initialization

⚠ ⚠ ⚠

⚠ Beware that this __will__ change critical parts and settings of the host.

⚠ This __will__ break things if there are any other services on the host!

⚠ _Use a dedicated machine (virtual or bare metal) for this._

⚠ ⚠ ⚠

Most settings are in the kukulkan.conf,  
but the init/install scripts will sometimes _interactively ask questions_.

Installation and initialization is split in those steps:

0. Download install script (manual copy/paste to your root cmd!)
    set hostname `CHANGEME`
    ```bash
    kukulkanHostname=CHANGEME
    domain=kukulkanctf.de
    ```
    
    copy deployment key from working station to the machine (in ubuntu to user-home)
    ```bash
    scp -p ~/.ssh/deployment $kukulkanHostname.$domain:.ssh/deployment
    scp -p ~/.ssh/deployment.pub $kukulkanHostname.$domain:.ssh/deployment.pub
    ssh $kukulkanHostname.$domain
    ```
    
    On host switch from user to root
    ```bash    
    sudo rsync -a ~/.ssh/deployment /root/.ssh/deployment
    sudo rsync -a ~/.ssh/deployment.pub /root/.ssh/deployment.pub
    sudo -i
    ```
    
    Prepare host and enable git
    ```bash
    printf "\nHost git.haw-hamburg.de\n  User git\n  Hostname git.haw-hamburg.de\n  IdentityFile /root/.ssh/deployment\n  StrictHostKeyChecking no\n\n" >> "/root/.ssh/config"
    apt-get update && apt-get upgrade -y && apt-get install -y git
    ```
    
    ```bash
    git clone git@git.haw-hamburg.de:uwe-ba/kukulkanctf.git ./kukulkan
    ```
    
    ```bash
    cd kukulkan
    ./initHost.sh
    ```

3. __reconnect to the host with the new port__

    `DISCONNECT` !  
    
    Then on your workstation (if variable not available??)
    ```bash
    kukulkanHostname=CHANGEME
    ```
    
    ```bash
    printf "\nHost $kukulkanHostname\n  Hostname $kukulkanHostname.$domain\n  User uwe\n  Port 22222\n  IdentityFile /home/uwe/.ssh/google_compute_engine\n\n" >> "/home/uwe/.ssh/config"
    ssh $kukulkanHostname
    ```

## Usage after installation

```bash
sudo -i
cd kukulkan
./kukulkan.sh
```
* creates and configure a container
 * ⚠ changes the iptables from the host to forward every port to this container
   expect the ssh port to the host, which is set in kukulkan.conf
* installs and configures services inside the container
* starts logging of all shell interaction inside the container

After initialization, the host is supposed to log every shell interaction that
happens inside the container.

## Further usage

When the container was initialized but the host had a restart, use  
`./afterRestart.sh`

When surveilance was initialized but logging needs restart, use  
`./startSurveilance.sh`

When the webside inside the container was initialized but its content needs
update, use  
`./populateGuestApacheWebsite.sh`

When a container was running but it should be replaced by a new one, use  
`./kukulkan.sh`


## Scripts

All scripts prefixed "init" are supposed to be executed exactly once
at system installation.  
(See "Initialization")

All scripts prefixed with "start" are supposed to be used manually.

All scripts prefixed "populate" are supposed to be used manually for population
or update of guest services.  
(For example downloading html files and pushing them into the container.)

## Customization

* If you need more ports from internet to host, change the code in forwardPorts.sh


## Risks
(as far as known)

Part of this game is to expect that potential attackers are smarter than me.  
Therefore DO NOT use this in a production environment.  
This is a research-only honeypot and not intended to be used as production  
honeypot, IDS or anything else.

The guest will get compromised.  
The guest root account will get compromised.  
Even if unprivileged lxc containers are expected to be "root-safe",
expect the host to get compromised.
Don't keep secrets on it and always keep a reliable way
for full system recovery/destruction.

⚠ ⚠ ⚠ Beware that Kukulkan __will__ change critical parts and settings
of the host.  
This _will_ break things if there are any other services on the host!  
_Use a dedicated machine (virtual or bare metal) for this._


## References
[1] https://docs.gitlab.com/ee/user/project/deploy_keys/index.html
